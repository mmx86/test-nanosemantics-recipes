// Create user to work with recipes_db


db = db.getSiblingDB('admin')
db.auth('adminuser', 'adminpass')

db = db.getSiblingDB('recipes_db')
db.createUser(
    {
        user: "user",
        pwd: "pass",
        roles: [
            {
                role: "readWrite",
                db:   "recipes_db"
            }
        ]
    }
);
