// Init database with provided recipes.

db = db.getSiblingDB('recipes_db')
db.auth('user', 'pass')

recipes = [
    {
        "name": "Салат «Русский»",
        "components": [
            {
                "item": "мясо",
                "q": 250
            },
            {
                "item": "огурец",
                "q": 2
            }
        ]
    },
    {
        "name": "Салат «Ленинградский»",
        "components": [
            {
                "item": "мясо",
                "q": 500
            },
            {
                "item": "картофель",
                "q": 3
            }
        ]
    },
    {
        "name": "Салат с рыбой и овощами",
        "components": [
            {
                "item": "рыба",
                "q": 500
            },
            {
                "item": "картофель",
                "q": 10
            },
            {
                "item": "яйцо",
                "q": 3
            }
        ]
    }
]
db.recipes.insertMany(recipes)
