FROM python:3.7

#ENV GROUP_ID=1000 USER_ID=1000

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
RUN pip install gunicorn

#RUN addgroup --gid $GROUP_ID www
#RUN adduser -D -u $USER_ID -G www www -s /bin/sh
#
#USER www

EXPOSE 5000

CMD ["gunicorn", "-w", "4", "-b", ":5000", "create_app:app"]
