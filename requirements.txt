environs==7.1.0
flask-caching==1.8.0
flask-marshmallow==0.10.1
flask-pymongo==2.3.0
flask==1.1.1
pytest==5.3.2
webtest==2.0.33
