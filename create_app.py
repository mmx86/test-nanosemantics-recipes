"""Create an application instance."""
from recipes.app import create_app

app = create_app()
