"""
Configuration of application.

Some of the configuration is set via environment variables.
"""
import os

from environs import Env


# Read '.env' file if exists
env = Env()
env.read_env()


# Directories
APP_DIR = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.dirname(APP_DIR)

# Flask configuration
ENV = env.str('FLASK_ENV', default='production')
DEBUG = ENV == 'development'
SECRET_KEY = env.str('SECRET_KEY', default='not-so-secret-key')

# Mongodb configuration
MONGO_URI = env.str('MONGO_URI', default='mongodb://localhost/recipes_db')

# Logging
LOGS_DIR = os.path.join(PROJECT_ROOT, 'logs')
LOGS_FILE = os.path.join(LOGS_DIR, 'recipes.log')
LOGS_CONFIG = {
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'filename': LOGS_FILE,
            'maxBytes': 10 * (1 << 20),  # 10 Mb
            'backupCount': 10,
        },
    },
    'root': {
        'level': env.str('LOG_LEVEL', default='ERROR'),
        'handlers': ['wsgi', 'file'],
    }
}
