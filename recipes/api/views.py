"""User views."""
from collections import defaultdict

from flask import (
    Blueprint,
    current_app as app,
    jsonify,
    request,
)
from flask_marshmallow import exceptions

from recipes.extensions import (
    mm,
    mongo,
)


blueprint = Blueprint("api", __name__, url_prefix="/api/v1")


class IngredientSchema(mm.Schema):
    """
    Ingredient schema.
    """
    item = mm.String(required=True)
    q = mm.Integer(required=True)


@blueprint.route("/recipes", methods=["GET"])
def get_available_recipes():
    """
    Returns all available recipes and number of portions for each.
    """
    app.logger.info(f'Processing API request: {request.url}')

    # Prepare args for validation.
    args = request.args
    raw_data = []
    for item in args:
        for q in args.getlist(item):
            raw_data.append({'item': item, 'q': q})

    # Validate args.
    try:
        ingredients = IngredientSchema(many=True).load(raw_data)
    except exceptions.ValidationError as err:
        app.logger.warning(
            f'Invalid API request: {request.url}. Msg:{err.messages}')
        msg = {raw_data[i][name]: item
               for i, error in err.messages.items()
               for name, item in error.items()}
        return msg, 422

    # Group up repeated ingredients.
    total = defaultdict(int)
    for i in ingredients:
        item = i['item']
        q = i['q']
        total[item] += q

    # Get recipes
    response = {}
    for recipe in mongo.db.recipes.find():
        max_dishes = None
        for component in recipe['components']:
            q = component['q']
            item = component['item']
            cur_dishes = total.get(item, 0) // q
            if max_dishes is None:
                max_dishes = cur_dishes
            else:
                max_dishes = min(max_dishes, cur_dishes)
            if cur_dishes <= 0:
                break
        if max_dishes is not None and max_dishes > 0:
            response[recipe['name']] = max_dishes

    return jsonify(response)
