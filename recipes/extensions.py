"""
Extensions module.

Each extension is initialized in the app factory in app.py.
"""
from flask_pymongo import PyMongo
from flask_marshmallow import Marshmallow


mongo = PyMongo()
mm = Marshmallow()
