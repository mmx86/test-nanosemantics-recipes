"""The module, containing the app factory function."""
import os

from logging.config import dictConfig

from flask import Flask

from recipes import api
from recipes.extensions import (
    mm,
    mongo,
)


def create_app(config="recipes.settings"):
    """Create application factory.

    :param config: The configuration to use.
    """
    app = Flask(__name__.split(".")[0])
    app.config.from_object(config)

    register_extensions(app)
    register_blueprints(app)

    configure_logger(app)
    return app


def register_extensions(app):
    """Register extensions."""
    mm.init_app(app)
    mongo.init_app(app)


def register_blueprints(app):
    """Register blueprints."""
    app.register_blueprint(api.views.blueprint)


def configure_logger(app):
    """Configure loggers."""
    LOGS_CONFIG = app.config['LOGS_CONFIG']

    # Create directory for log files
    LOGS_DIR = app.config['LOGS_DIR']
    if 'file' in LOGS_CONFIG['root']['handlers']:
        if not os.path.exists(LOGS_DIR):
            os.mkdir(LOGS_DIR)

    dictConfig(LOGS_CONFIG)
