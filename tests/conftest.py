"""Defines fixtures available to all tests."""

import json
import logging

import pytest

from webtest import TestApp

from recipes.app import create_app
from recipes.extensions import mongo


@pytest.fixture
def app():
    """Create application for the tests."""
    _app = create_app("tests.settings")
    _app.logger.setLevel(logging.CRITICAL)
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture
def testapp(app):
    """Create Webtest app."""
    return TestApp(app)


@pytest.fixture
def db(app):
    """Create database for the tests."""
    _db = mongo.db
    with app.app_context():
        with open('task.json', 'r') as f:
            json_data = json.load(f)

        for colname, docs in json_data.items():
            collection = _db.get_collection(colname)
            collection.insert_many(docs)

    yield _db

    _db.command('dropDatabase')
