"""Settings module for tests."""
from urllib.parse import urlsplit, urlunsplit

from recipes.settings import (
    LOGS_CONFIG,
    LOGS_DIR,
    MONGO_URI,
)


ENV = 'development'
TESTING = True
LOG_LEVEL = 'CRITICAL'
SECRET_KEY = "not-so-secret-in-tests"

# Use a separate database for testing.
uriparts = list(urlsplit(MONGO_URI))
uriparts[2] = '/tests_db'
MONGO_URI = urlunsplit(uriparts)

# Use only default stream handler for logging.
LOGS_CONFIG['root']['handlers'] = ['wsgi']
