"""Functional tests using WebTest.

See: http://webtest.readthedocs.org/
"""
from flask import url_for
from urllib.parse import urlencode


class TestAPIRecipes:
    """ API Recipes """

    def test_endpoint_is_alive(self, testapp, db):
        """ Successful response. """
        res = testapp.get(url_for("api.get_available_recipes"))

        assert res.status_code == 200

    def test_correct_response(self, testapp, db):
        """ Correct response. """
        qs = {
            'мясо': 250,
            'огурец': 2,
        }
        url = url_for("api.get_available_recipes") + '?' + urlencode(qs)
        res = testapp.get(url)

        assert res.status_code == 200
        assert res.json == {'Салат «Русский»': 1}

    def test_invalid_query(self, testapp, db):
        """ Incorect query string returns status code 422. """
        qs = {
            'мясо': 250,
            'огурец': 'два',
        }
        url = url_for("api.get_available_recipes") + '?' + urlencode(qs)

        testapp.get(url, status=422)

    def test_no_ingredients(self, testapp, db):
        """ No ingredients return empty dict. """
        qs = {
            'мясо': 0,
            'рыба': -1,
        }
        url = url_for("api.get_available_recipes") + '?' + urlencode(qs)
        res = testapp.get(url)

        assert res.json == {}

    def test_big_query(self, testapp, db):
        """ Big query string. """
        qs = {
            'мясо': 1000,
            'картофель': 1000,
            'огурец': 1000,
            'рыба': 1000,
            'яйцо': 1000,
        }
        qs.update({i: i for i in range(1000)})

        url = url_for("api.get_available_recipes") + '?' + urlencode(qs)
        res = testapp.get(url)

        assert res.json == {
            'Салат «Ленинградский»': 2,
            'Салат «Русский»': 4,
            'Салат с рыбой и овощами': 2
        }

    def test_repeated_ingredients(self, testapp, db):
        """ Repeated ingredients add up. """
        qs = (
            ('огурец', 1),
            ('мясо', 250),
            ('огурец', 1),
        )
        url = url_for("api.get_available_recipes") + '?' + urlencode(qs)
        res = testapp.get(url)

        assert res.json == {'Салат «Русский»': 1}
