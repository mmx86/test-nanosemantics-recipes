"""
Initialize database with given recipes.
"""
import json

from recipes.app import create_app, mongo


app = create_app()
db = mongo.db

with open('task.json', 'r') as f:
    json_data = json.load(f)

for colname, docs in json_data.items():
    collection = mongo.db.get_collection(colname)
    collection.drop()
    collection.insert_many(docs)
